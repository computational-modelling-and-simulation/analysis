# Dynamical modelling of type I IFN  responses in SARS-CoV2 infection

## Contributors:
* Anna Niarakis, anna.niaraki@univ-evry.fr,
* Sara Sadat Aghamiri,saghamiri2@unl.edu
* Vidisha Singh,vidishasingh68@gmail.com
* Aurelien Naldi, aurelien.naldi@inria.fr
* Sylvain Soliman,sylvain.soliman@inria.fr
* Bhanwar Lal Puniya, bpuniya2@unl.edu
* Tomas Helikar, thelikar2@unl.edu
* Ahmed Abdelmonem Hemedan, ahmed.hemedan@uni.lu 

## Scope: 
* In this work, we make use of the type I IFN graphical model developed during the COVID19 Disease Map project and available in the C19DM repository (references) and the map-to-model translation framework developed in (http://dx.doi.org/10.1093/bioinformatics/btaa484), to obtain an executable, dynamic model of type I IFN signalling for in silico experimentation. Our goal is to understand how we can maximize the Antiviral Immune response (production of ISGs), limit Inflammation, and amplify the inhibition of viral replication through the production of the IFNs. 

## Methodology: 

**Creating the executable Boolean model:** 
* Starting from the CellDesigner map of the type I Interferon signalling we use the tool CaSQ (https://gitlab.inria.fr/soliman/casq) to obtain an SBML qual file with preliminary Boolean rules. To cope with specificities of the COVID19 Disease Map repository, the tool CaSQ was adapted (version 9.0.2) to include: 
a) Automatic merging of identical SBML species that can appear for visual purposes on the XML CellDesigner graph
b) The active suffix in the species name when the dotted circle was used to denote activation of a certain species.
The SBML qual file is then analyzed using the following modelling software: Cell Collective, GINsim and BoolNet for input-output relations, real-time simulations, sensitivity analysis and attractors. 

**Pre-processing modifications of the CellDesigner graph:**
* Feedback loops were added according to the literature: 
a) The autocrine loop of the secreted IFNA1 and IFNB1 proteins, responsible for the amplification of the signal regarding the IFN+IFNR complex formation 
b) The inhibition of viral replication via the secreted IFNA1 and IFNB1 proteins 

**Pre-processing step for the attractors search:**
* The SBML qual file was processed to ensure compatibility regarding name display in GINsim, and also control nodes were added in an attempt to reduce the number of inputs (from 37 to 7) and ease the computational cost. 

**Real time simulations:**
* The sbml qual file was imported to Cell Collective. The platform was used to simulate eight scenarios derived from literature. 

**Attractors identification:**
* Given the size of the model (given the number of nodes and edges), an exhaustive attractors search would not be feasible. Therefore, we searched for attractors for a given set of initial conditions that cover different biological scenarios of the type I IFN pathway with or without the infection, and in the presence or absence of drugs.

**CoLoMoTo notebook implementation:** 
+++

**Sensitivity analysis against overexpression and knockout mutations :**
* We analyzed the IFN1-based sensitivity by efficiently employing knockout and overexpression gene mutations. Besides, we specified the mutation area in a phenotypic wise manner. The sensitivity computation can be executed in a parallel algorithm using the OpenCL library to fasten the sensitivity analysis in all boolean nodes and network modules. 